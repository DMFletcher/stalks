import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
// import registerServiceWorker from './registerServiceWorker';

// Commented out reacts service worker, if we want to use one, we will write our own

ReactDOM.render(<App />, document.getElementById('root'));
// registerServiceWorker();
